/* Feel free to use this example code in any way
   you see fit (Public Domain) */

#include <sys/types.h>
#ifndef _WIN32
#include <sys/select.h>
#include <sys/socket.h>
#else
#include <winsock2.h>
#endif
#include <string.h>
#include <microhttpd.h>
//#include <internal.h>
#include <stdio.h>
#include <stdlib.h>
#include <gnunet/platform.h>
#include <gnunet/gnunet_pq_lib.h>
#include <gnunet/gnunet_util_lib.h>

#define PORT 8888
#define POSTBUFFERSIZE  512

enum http_method
{
  GET,
  POST,
  DELETE,
  OTHER
};

struct session_info
{
  enum http_method *req_method;
  char *response_page;
  unsigned int status_code;
  char *api_version;
  char *user;
  char *file_uuid;
  char *cbox_filehash;
  char *cbox_filename;
  char *expect;
  size_t *file_size;
  char *file_data;
  struct MHD_PostProcessor *file_postprocessor;
  FILE *file_fp;
};
typedef struct session_info Session_Info;

struct session_info make_session_info(struct MHD_Connection *connection, char *method, char *response_page, char *called_url, char *user,size_t *file_size, char *file_data)
{
  /* create an empty struct with all members = NULL */
  struct session_info temp;
  temp.req_method = NULL;
  temp.response_page = NULL;
  temp.status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
  temp.api_version = NULL;
  temp.user = NULL;
  temp.file_uuid = NULL;
  temp.cbox_filehash = NULL;
  temp.cbox_filename = NULL;
  temp.expect = NULL;
  temp.file_size = file_size;
  temp.file_data = file_data;
  temp.file_postprocessor = NULL;
  temp.file_fp = NULL;

  temp.response_page = response_page;
  char *temp_url = strdup(called_url);
  struct session_info url;
  char *slash, *api, *users, *files;
  int url_length = strlen(called_url);
  char last_char = called_url[url_length - 1];

  /* parse http method */
  int method_get_cmp_res = strcmp(method, "GET");
  int method_post_cmp_res = strcmp(method, "POST");
  int method_delete_cmp_res = strcmp(method, "DELETE");
  if (method_get_cmp_res == 0)
  {
    temp.req_method = GET;
  }
  else if (method_post_cmp_res == 0)
  {
    temp.req_method = POST;
  }
  else if (method_delete_cmp_res == 0)
  {
    temp.req_method = DELETE;
  }
  else
  {
    temp.req_method = OTHER;
  }

  /* remove slash at the end if one is there */
  if (last_char == '/')
  {
    temp_url[url_length - 1] == '\0';
    url_length--;
  }

  /* count slashes in the url */
  int num_slashes = 1;
  for (int i = 0; i < url_length; i++)
  {
    if (temp_url[i] == '/')
    {
      num_slashes++;
    }
  }

  char *url_parts[num_slashes];
  for (int i = 0; i < num_slashes; i++)
  {
    url_parts[i] = strsep(&temp_url, "/");
  }

  // plausibility tests
  bool is_api;
  bool is_supported_api_version;
  bool is_users;
  bool is_user_legit;
  bool is_files;
  bool is_method_valid;
  if (num_slashes >= 6)
  {
    is_api = !strcmp("api", url_parts[1]);
    is_supported_api_version = !strcmp("v1", url_parts[2]);
    is_users = !strcmp("users", url_parts[3]);
    is_user_legit = !strcmp(user, url_parts[4]);
    is_files = !strcmp("files", url_parts[5]);
    is_method_valid = !(temp.req_method == OTHER);
  }

  if (is_api && is_supported_api_version && is_users && is_user_legit && is_files && is_method_valid)
  {
    temp.api_version = url_parts[2];
    temp.user = url_parts[4];
  }

  if (is_files && num_slashes == 7)
  {
    temp.file_uuid = url_parts[6];
  }
  return temp;
}

PGconn *conn;

// todo
// add sql prepared statements struct

char *readLocalFile(char *fileName)
{
  FILE *file = fopen(fileName, "rb");
  char *code;
  size_t n = 0;
  int c;

  if (file == NULL)
    return NULL; //could not open file
  fseek(file, 0, SEEK_END);
  long f_size = ftell(file);
  fseek(file, 0, SEEK_SET);
  code = malloc(f_size);

  while ((c = fgetc(file)) != EOF)
  {
    code[n++] = (char)c;
  }
  code[n] = '\0';

  return code;
}

static int
iterate_post (void *coninfo_cls,
              enum MHD_ValueKind kind,
              const char *key,
              const char *filename,
              const char *content_type,
              const char *transfer_encoding,
              const char *data,
              uint64_t off,
              size_t size)
{
  struct session_info *con_info = coninfo_cls;
  FILE *fp;
  (void)kind;               /* Unused. Silent compiler warning. */
  (void)content_type;       /* Unused. Silent compiler warning. */
  (void)transfer_encoding;  /* Unused. Silent compiler warning. */
  (void)off;                /* Unused. Silent compiler warning. */

  if (0 != strcmp (key, "file"))
    {
      con_info->response_page = "Error while file iterates, key section";
      con_info->status_code = MHD_HTTP_BAD_REQUEST;
      return MHD_YES;
    }

  if (! con_info->file_fp)
    {
      if (0 != con_info->status_code) /* something went wrong */
        return MHD_YES;
      if (NULL != (fp = fopen (filename, "rb")))
        {
          fclose (fp);
          con_info->response_page = "File already exists";
          con_info->status_code = MHD_HTTP_FORBIDDEN;
          return MHD_YES;
        }
      /* NOTE: This is technically a race with the 'fopen()' above,
         but there is no easy fix, short of moving to open(O_EXCL)
         instead of using fopen(). For the example, we do not care. */
      con_info->file_fp = fopen (filename, "ab");
      if (!con_info->file_fp)
        {
          con_info->response_page = "File IO Error";
          con_info->status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
          return MHD_YES;
        }
    }

  if (size > 0)
    {
      if (! fwrite (data, sizeof (char), size, con_info->file_fp))
        {
          con_info->response_page = "File IO Error";
          con_info->status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
          return MHD_YES;
        }
    }

  return MHD_YES;
}

static void
request_completed (void *cls,
                   struct MHD_Connection *connection,
                   void **con_cls,
                   enum MHD_RequestTerminationCode toe)
{
  struct session_info *con_info = *con_cls;
  (void)cls;         /* Unused. Silent compiler warning. */
  (void)connection;  /* Unused. Silent compiler warning. */
  (void)toe;         /* Unused. Silent compiler warning. */

  if (NULL == con_info)
    return;

  if (con_info->req_method == POST)
    {
      if (NULL != con_info->file_postprocessor)
        {
          MHD_destroy_post_processor (con_info->file_postprocessor);
        }

      if (con_info->file_fp)
        fclose (con_info->file_fp);
    }

  free (con_info);
  *con_cls = NULL;
}


/** 
* Just opens the connection to the postgre DB
*/
PGconn *setup_database()
{
  GNUNET_log_setup("cbox", "DEBUG", NULL);

  PGconn *conn = GNUNET_PQ_connect("postgresql://postgres:1234qwer@localhost/cbox");

  struct GNUNET_PQ_PreparedStatement ps[] = {
      GNUNET_PQ_make_prepare("password_select", "SELECT password FROM account WHERE username = $1;", 1),
      GNUNET_PQ_make_prepare("files_select", "SELECT file.id,file.name,file.hash FROM account INNER JOIN file ON file.username = account.username WHERE account.username = $1;", 1),
      GNUNET_PQ_make_prepare("file_select", "SELECT file.id,file.name,file.hash FROM account INNER JOIN file ON file.username = account.username WHERE account.username = $1 AND file.id = $2;", 2),
      GNUNET_PQ_make_prepare("file_hash", "SELECT file.hash FROM account INNER JOIN file ON file.username = account.username WHERE account.username = $1 AND file.id = $2;", 2),
      GNUNET_PQ_make_prepare("fileData_select", "SELECT file.data FROM account INNER JOIN file ON file.username = account.username WHERE account.username = $1 AND file.id = $2;", 2),
      GNUNET_PQ_make_prepare("insert_file", "INSERT INTO file (id,name,hash,data,username) VALUES ($1,$2,$3,$4,$5);", 5),
      GNUNET_PQ_make_prepare("set_file", "UPDATE file SET name = $2, hash = $3, data = $4, username = $5 WHERE id = $1 AND username = $5", 5),
      GNUNET_PQ_make_prepare("delete_file", "DELETE FROM file WHERE id = $1 AND username = $2;", 2),

      GNUNET_PQ_PREPARED_STATEMENT_END};

  GNUNET_PQ_prepare_statements(conn, ps);

  return conn;
}

char *get_password(PGconn *conn, char *username)
{
  struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string(username),
      GNUNET_PQ_query_param_end};

  char *val = NULL;
  size_t val_size;
  struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_variable_size("password", (void **)&val, &val_size),
      GNUNET_PQ_result_spec_end};

  GNUNET_PQ_eval_prepared_singleton_select(conn, "password_select", params, rs);

  char *newval = NULL;

  if (NULL != val)
  {
    if (val[0] != '\0')
    {
      newval = malloc(val_size);

      strcpy(newval, val);

      GNUNET_PQ_cleanup_result(rs);
    }
    else
    {
      newval = NULL;
    }
  }
  else
  {
    newval = NULL;
  }

  return newval;
}

char *get_file(PGconn *conn, Session_Info *si)
{
  struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string(si->user),
      GNUNET_PQ_query_param_string(si->file_uuid),
      GNUNET_PQ_query_param_end};

  char *val_uuid;
  size_t val_uuid_size;
  char *val_name;
  size_t val_name_size;
  char *val_hash;
  size_t val_hash_size;

  struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_variable_size("id", (void **)&val_uuid, &val_uuid_size),
      GNUNET_PQ_result_spec_variable_size("name", (void **)&val_name, &val_name_size),
      GNUNET_PQ_result_spec_variable_size("hash", (void **)&val_hash, &val_hash_size),
      GNUNET_PQ_result_spec_end};

  int queryStatus = GNUNET_PQ_eval_prepared_singleton_select(conn, "file_select", params, rs);

  char *newval = NULL;

  if (queryStatus == 1)
  {
    newval = malloc((val_name_size + val_hash_size + val_uuid_size) + 30);

    strcpy(newval, "uuid;name;hash\n");
    
    strcat(newval, val_uuid);
    strcat(newval, ";");
    strcat(newval, val_name);
    strcat(newval, ";");
    strcat(newval, val_hash);

    GNUNET_PQ_cleanup_result(rs);
  }

  return newval;
}

char *check_file_hash(PGconn *conn, Session_Info *si)
{
  struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string(si->user),
      GNUNET_PQ_query_param_string(si->file_uuid),
      GNUNET_PQ_query_param_end};

  char *val_hash;
  size_t val_hash_size;

  struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_variable_size("hash", (void **)&val_hash, &val_hash_size),
      GNUNET_PQ_result_spec_end};

  int queryStatus = GNUNET_PQ_eval_prepared_singleton_select(conn, "file_hash", params, rs);

  char *newval = NULL;

  if (queryStatus == 1)
  {
    newval = malloc(val_hash_size);

    memcpy(newval, val_hash, val_hash_size);

    GNUNET_PQ_cleanup_result(rs);
  }

  return newval;
}

char *get_fileData(PGconn *conn, char *username, char *uuid)
{
  struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string(username),
      GNUNET_PQ_query_param_string(uuid),
      GNUNET_PQ_query_param_end};

  char *val_data = NULL;
  size_t val_data_size = NULL;

  struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_variable_size("data", (void **)&val_data, &val_data_size),
      GNUNET_PQ_result_spec_end};

  int queryStatus = GNUNET_PQ_eval_prepared_singleton_select(conn, "fileData_select", params, rs);

  char *newval = NULL;

  if (queryStatus == 1)
  {
    newval = malloc(val_data_size);

    memcpy(newval, val_data, val_data_size);

    GNUNET_PQ_cleanup_result(rs);
  }

  return newval;
}

int insert_file(PGconn *conn, Session_Info *si)
{
  struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string(si->file_uuid),
      GNUNET_PQ_query_param_string(si->cbox_filename),
      GNUNET_PQ_query_param_string(si->cbox_filehash),
      GNUNET_PQ_query_param_string(si->file_data),
      GNUNET_PQ_query_param_string(si->user),
      GNUNET_PQ_query_param_end};

  int queryStatus = NULL;

  if (NULL == get_file(conn, si))
  {
    queryStatus = GNUNET_PQ_eval_prepared_non_select(conn, "insert_file", params);
  }
  else
  {
    queryStatus = GNUNET_PQ_eval_prepared_non_select(conn, "set_file", params);
  }
  //id,name,hash,data,username
  //printf("%s %s %s %s %s"\n,uuid,filename,filehash,username,data);

  return queryStatus;
}

int delete_file(PGconn *conn, Session_Info *si)
{
  //id,name,hash,data,username
  struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string(si->file_uuid),
      GNUNET_PQ_query_param_string(si->user),
      GNUNET_PQ_query_param_end};

  int queryStatus = GNUNET_PQ_eval_prepared_non_select(conn, "delete_file", params);

  return queryStatus;
}

static void hanlde_files_results(void *cls, PGresult *result, unsigned int num_results)
{
  char *val_uuid;
  size_t val_uuid_size;
  char *val_name;
  size_t val_name_size;
  char *val_hash;
  size_t val_hash_size;

  char *newval = cls;

  if (PQntuples(result) != 0)
  {

    int length = 0;

    for (unsigned int i = 0; i < PQntuples(result); ++i)
    {
      for (unsigned int j = 0; j < PQnfields(result); ++j)
      {
        length += PQgetlength(result, i, j);
      }
    }

    snprintf(newval, (PQntuples(result) * 20) + length, "%s;%s;%s\n", "uuid", "name", "hash");

    for (unsigned int i = 0; i < num_results; i++)
    {

      struct GNUNET_PQ_ResultSpec rs[] = {
          GNUNET_PQ_result_spec_variable_size("id", (void **)&val_uuid, &val_uuid_size),
          GNUNET_PQ_result_spec_variable_size("name", (void **)&val_name, &val_name_size),
          GNUNET_PQ_result_spec_variable_size("hash", (void **)&val_hash, &val_hash_size),
          GNUNET_PQ_result_spec_end};
      GNUNET_PQ_extract_result(result, rs, i);
      //printf("name: %s | hash %s \n", val_name, val_hash);
      strcat(newval, val_uuid);
      strcat(newval, ";");
      strcat(newval, val_name);
      strcat(newval, ";");
      strcat(newval, val_hash);
      strcat(newval, "\n");
      GNUNET_PQ_cleanup_result(rs);
    }
  }
  else
  {
    snprintf(newval, 20, "%s;%s\n", "name", "hash");
  }
  //printf("Inside:\n%s\n",newval);
}

char *get_files(PGconn *conn, Session_Info *si)
{
  struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string(si->user),
      GNUNET_PQ_query_param_end};

  char *myResult = malloc(sizeof(char) * 1024); // ToDo:Fix for dynamic allocation

  GNUNET_PQ_eval_prepared_multi_select(conn, "files_select", params, &hanlde_files_results, myResult);
  //printf("Outside:\n%s\n",myResult);

  return myResult;
}

/** 
* Entrypoint for the http request on MHD
*
* @return  Integer as status. 0 = OK, 1 = NOK
*/
static int process_api_call(Session_Info *si)
{
  int return_status = -1;
  int api_type = 0; //todo

  if (si->req_method == GET)
  {
    if (si->file_uuid)
    {
      printf("call get_file_v1(..)\n");
      si->response_page = get_file(conn, si);
      if (si->response_page == NULL)
      {
        si->status_code = MHD_HTTP_NOT_FOUND;
        si->response_page = "404 Not found";
        return_status = 0;
      }
      else
      {
        si->status_code = MHD_HTTP_OK;
        return_status = 0;
      }
    }
    else
    {
      printf("call get_files_v1(..)\n");
      si->response_page = get_files(conn, si);
      si->status_code = MHD_HTTP_OK;
      return_status = 0;
    }
  }
  else if (si->req_method == POST)
  {
    if (si->file_uuid && si->expect)
    {
      char *current_fhash = check_file_hash(conn, si);
      if (0 == strcmp(current_fhash, si->cbox_filehash))
      {
        printf("file already in cbox store\n");
        si->status_code = MHD_HTTP_EXPECTATION_FAILED;
        si->response_page = "417 Expectation failed - File is alredy in store!";
        return_status = 0;
      }
      else
      {
        printf("file needs to be uploaded\n");
        //si->status_code = MHD_HTTP_CONTINUE;
        //si->status_code = MHD_HTTP_EXPECTATION_FAILED;
        si->response_page = "100 Continue - Please send file";
        //return MHD_YES;
        return_status = 0;
      }
    }
    else if (si->file_uuid)
    {
      //printf("call post_file_v1(..)\n");
      if (0 != *si->file_size){
        si->status_code = 0;
        int retPostProcess = MHD_post_process(si->file_postprocessor,si->file_data,*(si->file_size));
        *(si->file_size) = 0;
        return MHD_YES;
      } 
      /* Upload finished */
      if (NULL != si->file_fp)
        {
          fclose (si->file_fp);
          si->file_fp = NULL;
        }
      if (0 == si->status_code)
        {
          /* No errors encountered, declare success */
          si->response_page = "File uploaded";
          si->status_code = MHD_HTTP_OK;
        }
      return_status = 0;
      
    }
    else
    {
      printf("400 Bad request - no UUID provided!\n");
      si->status_code = MHD_HTTP_BAD_REQUEST;
      si->response_page = "400 Bad request - no UUID provided!";
    }
  }
  else if (si->req_method == DELETE)
  {
    if (si->file_uuid)
    {
      printf("call delete_file_v1(..)\n");
      delete_file(conn, si);
      si->response_page = "File deleted";
      si->status_code = MHD_HTTP_OK;
      return_status = 0;
    }
    else
    {
      printf("No API call fot this URI\n");
    }
  }
  else
  {
    printf("No API call fot this URI\n");
  }
  return return_status;
}

/** 
* Returns a list of stored files in cBox
*
* @param *user_id  The id of the user whose requests the file list
* @return          Integer as status. 0 = OK, 1 = NOK
*/
int get_files_v1(char *user_id)
{
  printf("Returned Multi %s\n", get_files(conn, "michel"));
  return 0;
}

/** 
* Prepares and sends the file as http body response
*
* @param *user_id    User id to specify the cBox account
* @param *file_uuid  The unique file to transfere to the requester
* @return            Integer as status. 0 = OK, 1 = NOK
*/
int get_file_v1(char *user_id, char *file_uuid)
{
  return 0;
}

/** 
* Receives a file via http and call the DB store function
*
* @param *user_id    User id to specify the cBox account
* @param *file_uuid  The unique file id that serves as key int the store.
* @return            Integer as status. 0 = OK, 1 = NOK
*/
int post_file_v1(char *user_id, char *file_uuid, char *file_hash)
{
  // todo
  // check if file is already in the store -> based on sha256 hash
  // if yes send a http-417
  // else send a http-100 and prepare for receiving data.

  return 0;
}

/** 
* Deletes a specific file in the cBox store
*
* @param *user_id    User id to specify the cBox account
* @param *file_uuid  The unique file id that serves as key int the store.
* @return            Integer as status. 0 = OK, 1 = NOK
*/
int delete_file_v1(char *user_id, char *file_uuid)
{
  return 0;
}

/** dddddd
* Gets a file from the database backend.
* Idea: Return a pointer to the data.
*
* @param *user_id    User id to specify the cBox account
* @param *file_uuid  The unique file id that serves as key int the store.
* @return            Integer as status. 0 = OK, 1 = NOK
*/
int get_file_from_db(char *user_id, char *file_uuid)
{
  return 0;
}

/** 
* Stores a file into the DB backend
*
* @param *user_id    User id to specify the cBox account
* @param *file_uuid  The unique file id that serves as key int the store.
* @return            Integer as status. 0 = OK, 1 = NOK
*/
int post_file_in_db(char *user_id, char *file_uuid)
{
  return 0;
}

/** 
* Deletes a file for a specific user in the database
*
* @param *user_id  The unique id of the user
* @return          Integer as status. 0 = OK, 1 = NOK
*/
int delete_file_in_db(char *user_id, char *file_uuid)
{
  return 0;
}

/**
 * Method to check username and password
 * 
 * @param *user   The unique id of the user
 * @parem *pass   The password of the given user
 */
bool authentication_ok(char *user, char *pass)
{
  if (user == NULL || strlen(user) < 1)
  {
    return false;
  }
  else
  {
    char *users_pwd_real = get_password(conn, user);

    if (users_pwd_real == NULL)
    {
      return false;
    }

    int check_pwd_result = strcmp(pass, users_pwd_real);
    if (check_pwd_result == 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}

static int
get_custom_headers(void *cls, enum MHD_ValueKind kind, const char *key,
                   const char *value)
{
  struct session_info *sessionInfo = (struct session_info *)cls; /* Unused. Silent compiler warning. */
  (void)kind;                                                    /* Unused. Silent compiler warning. */
  //printf("%s: %s\n", key, value);

  int check_is_cbox_filehash_header = strcmp(key, "CBox-filehash");
  int check_is_cbox_filename_header = strcmp(key, "CBox-filename");
  int check_is_expect_header = strcmp(key, "Expect");
  if (check_is_cbox_filehash_header == 0)
    sessionInfo->cbox_filehash = value;
  if (check_is_cbox_filename_header == 0)
    sessionInfo->cbox_filename = value;
  if (check_is_expect_header == 0)
    sessionInfo->expect = value;

  return MHD_YES;
}

/** 
* Start answering the request
*/
static int answer_to_connection(void *cls, struct MHD_Connection *connection,
                                const char *url, const char *method,
                                const char *version, const char *upload_data,
                                size_t *upload_data_size, void **con_cls)
{
  char *pass = NULL;
  char *user = MHD_basic_auth_get_username_password(connection, &pass);
  int ret;
  struct MHD_Response *response;

  if (NULL == *con_cls){
    if (!authentication_ok(user, pass))
    {
      const char *page = "Credentials are not valid!";
      response = MHD_create_response_from_buffer(strlen(page), (void *)page, MHD_RESPMEM_PERSISTENT);
      ret = MHD_queue_basic_auth_fail_response(connection, "my realm", response);
      return ret;
    }
  } 


  const char *page;
  struct session_info *user_session_info = malloc(sizeof(struct session_info));
  if (NULL == *con_cls){
    *user_session_info = make_session_info(connection, method, page, url, user, upload_data_size, upload_data);
    *con_cls = (void *) user_session_info;
    if (user_session_info->req_method == POST){
      user_session_info->file_postprocessor = MHD_create_post_processor (connection,POSTBUFFERSIZE,&iterate_post,(void *) user_session_info);
      return MHD_YES; // ToDo Move to correct place!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    } 
  }else
  {
    user_session_info = *con_cls;
    user_session_info->file_size = upload_data_size;
    user_session_info->file_data = upload_data;
  }
  

  if (user_session_info->user == NULL)
  {
    const char *page = "Sorry, malformatted URL request!";
    response = MHD_create_response_from_buffer(strlen(page), (void *)page, MHD_RESPMEM_PERSISTENT);
    //ret = MHD_queue_basic_auth_fail_response(connection, "my realm", response);
    ret = MHD_queue_response(connection, MHD_HTTP_BAD_REQUEST, response);
  }
  else
  {
    //printf("cooool\n");
    MHD_get_connection_values(connection, MHD_HEADER_KIND, get_custom_headers, user_session_info);
    int retApiCall = process_api_call(user_session_info);
    
    
    if (MHD_YES == retApiCall){
      return MHD_YES;
    } 
    
    page = user_session_info->response_page;

    response = MHD_create_response_from_buffer(strlen(page), (void *)page, MHD_RESPMEM_PERSISTENT);
    //MHD_add_response_header(response, "Content-Type", "application/octet-stream");
    ret = MHD_queue_response(connection, user_session_info->status_code, response);
  }

  (void)cls;              /* Unused. Silent compiler warning. */
  (void)url;              /* Unused. Silent compiler warning. */
  (void)method;           /* Unused. Silent compiler warning. */
  (void)version;          /* Unused. Silent compiler warning. */
  (void)upload_data;      /* Unused. Silent compiler warning. */
  (void)upload_data_size; /* Unused. Silent compiler warning. */
  //(void)con_cls;          /* Unused. Silent compiler warning. */

  MHD_destroy_response(response);

  return ret;
}

int main(void)
{
  struct MHD_Daemon *daemon;

  conn = setup_database();
  
  daemon = MHD_start_daemon(MHD_USE_AUTO | MHD_USE_INTERNAL_POLLING_THREAD, PORT, NULL, NULL,
                            &answer_to_connection, NULL, MHD_OPTION_NOTIFY_COMPLETED, &request_completed, NULL,MHD_OPTION_END);
  if (NULL == daemon)
    return 1;

  (void)getchar();

  MHD_stop_daemon(daemon);
  return 0;
}
