# API Documentation

- [API Documentation](#api-documentation)
  - [Methods](#methods)
    - [Listing](#listing)
      - [Example listing](#example-listing)
    - [Download](#download)
      - [Example download](#example-download)
    - [Upload](#upload)
      - [Header-fields](#header-fields)
      - [Body data (actuel file)](#body-data-actuel-file)
      - [http body (as application/x-www-form-urlencoded)](#http-body-as-applicationx-www-form-urlencoded)
      - [Description upload](#description-upload)
        - [Check existens first](#check-existens-first)
        - [Just upload it](#just-upload-it)
      - [Example upload](#example-upload)
    - [Delete](#delete)
      - [Example delete](#example-delete)

## Methods

### Listing

    GET /api/v1/users/<user-id>/files

Returns a list of all files from the corresponding user `<user-id>`.

#### Example listing

```shell
curl -u hans:1234 http://host/api/v1/users/hans/files
```

Returns a CSV list:

```shell
uuid;name;hash
124169B8-5557-4E29-A29E-61C8CE3501C2;myfile.txt;358dc8f2a872b4563e8a415b836da450
F3271241-C231-4152-B63C-CB7FF5C26DA4;myproject.c;4bf9f805234b14da24b7e89bbc5d21dd
7E7DFB78-B923-479C-BA05-5E57A54A7582;work.docx;4ff1bcd426226b71de1b1002b80cab8c
```

### Download

    GET /api/v1/users/<user-id>/files/<file-uuid>

Starts download of a file (`<file-uuid>`)

#### Example download

```shell
curl -o myfile.txt -u hans:1234 http://host/api/v1/users/hans/files/124169B8-5557-4E29-A29E-61C8CE3501C2
```

Returns the requested binary.

### Upload

    POST /api/v1/users/<user-id>/files/<file-uuid>

#### Header-fields

| Key             | Value            | Description                             | Optional              |
| --------------- | ---------------- | --------------------------------------- | --------------------- |
| `CBox-filehash` | `<hash of file>` | file-hash calculeted with sha256        | no                    |
| `CBox-filename` | `<name of file>` | actual name of the upload file          | no                    |
| `Expect`        | `100-continue`   | see [Upload workflow](#Upload-workflow) | yes (but recommended) |

#### Body data (actuel file)

Files have to be uploaded as form-data with the key `file` and path to the passing file like `@myfilename`.

#### http body (as application/x-www-form-urlencoded)

File of your choice on your local disk.

#### Description upload

A client has two options to put a file into the CBox store.
Pushing a file according to [Upload](#Upload) API call can be with or without the `Expect` header.
But only with the `Expect` flag set can a unnecessary upload be avoided.

##### Check existens first

A CBox client can send an `Expect: 100-continue` header with the POST request. If the cBox-Server answerse with an `http 100` code the body (payload/file) can be transfered. If, according to the provided filehash, cBox knows already this file then a `http 417 (Expectation Failed)` code will be send back and no upload to cBox is needed.

This requires a http version >=1.1 connection.

##### Just upload it

In this case cBox will not checks if the file is already uploaded and writs it directly to the backend store.

It is recomended to use the check first method rather than just upload a file especially if the filesize is big.

#### Example upload

```shell
curl --request POST \
  --user hans:1234 \
  --url http://host/api/v1/users/hans/files/5079324D-2288-465A-92B5-6E901287E303 \
  --header 'CBox-filehash: 3cbc5db1110d07555f7a084ef199e80afb8e3b48' \
  --header 'CBox-filename: letter.docx' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --header 'Expect: 100-continue' \
  --header 'content-type: multipart/form-data' \
  --form file=@/home/hans/letter.docx
```

### Delete

    DELETE /api/v1/users/<user-id>/files/<file-uuid>

#### Example delete

```shell
curl --request DELETE \
  --user hans:1234 \
  --url http://host/api/v1/users/hans/files/5079324D-2288-465A-92B5-6E901287E303
```
