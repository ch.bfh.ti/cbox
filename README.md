# cbox

C-Project in BTI 7252 Development of Mobile Applications (HS 2018)
It's a REST-Webservice for upload and download files.

## Other considerations

### Use JSON as wrapper

There was an idea originally to wrap the payload in a json file an then push that as http body to the cBox server.
But since we have decided to use filehashes to compare files and send those with the header there is no other interesting metadata having to send with the payload.
Therefore files are send directly as http payload.