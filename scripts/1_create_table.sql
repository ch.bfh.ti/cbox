CREATE TABLE account
(
    username text COLLATE pg_catalog."default" NOT NULL,
    password text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "account_pkey" PRIMARY KEY (username)
);


CREATE TABLE file
(
    id text COLLATE pg_catalog."default" NOT NULL,
    username text COLLATE pg_catalog."default" NOT NULL,
    hash text COLLATE pg_catalog."default" NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    data bytea NOT NULL,
    CONSTRAINT "file_pkey" PRIMARY KEY (id),
    CONSTRAINT "FK_file_username_of_account_username" FOREIGN KEY (username)
        REFERENCES account (username) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);