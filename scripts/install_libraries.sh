#!/bin/bash

# This is a tiny helper script to download and install all the needed dependencies for this project.

function blueText {
    printf "\e[44m $1 \e[0m \n"
}


if [[ $(id -u) -ne 0 ]] ; then blueText "Please run with sudo" ; exit 1 ; fi


declare -a dependencies=(
    "autoconf"
    "automake"
    "bluez"
    "curl"
    "dh-autoreconf"
    "gettext"
    "git"
    "gnunet-dev"
    "gnutls-bin"
    "libcurl4-gnutls-dev"
    "libextractor-dev"
    "libgcrypt20-dev"
    "libgnutls28-dev"
    "libgstreamer1.0-0"
    "libidn2-0"
    "libjansson-dev"
    "libjansson-doc"
    "libltdl-dev"
    "libminiupnpc-dev"
    "libopus-dev"
    "libpq-dev"
    "libpulse-dev"
    "libtool"
    "libunistring-dev"
    "mysql-client"
    "mysql-server"
    "openssl"
    "pkg-config"
    "postgresql"
    "postgresql-contrib"
    "sqlite mysql-server"
    "texi2html"
    "texinfo"
    "zlib1g-dev"
)


blueText "Update APT..."
sudo apt update


blueText "Installing dependencies..."
if [ "$1" = "singleapt" ]; then
  for dependency in ${dependencies[@]}; do
    sudo apt install $dependency -y
  done
else
  all_packages=''
  for dependency in ${dependencies[@]}; do
    all_packages="$all_packages $dependency"
  done
  sudo apt install $all_packages -y
fi


blueText "Download software sources..."
sudo mkdir /opt/downl
cd /opt/downl
git clone git://git.gnunet.org/gnunet --ipv4
git clone git://git.taler.net/exchange --ipv4
git clone git://git.taler.net/twister --ipv4
wget https://ftp.gnu.org/gnu/gnunet/gnurl-7.53.1.tar.xz
tar -xf gnurl-7.53.1.tar.xz
wget https://mirror.kumi.systems/gnu/libmicrohttpd/libmicrohttpd-0.9.62.tar.gz
tar -xvf libmicrohttpd-0.9.62.tar.gz
sudo chown -R $SUDO_USER:$SUDO_USER /opt/downl

blueText "Install Gnurl..."
cd /opt/downl/gnurl-7.53.1
sudo mkdir /opt/gnurl
./configure --prefix=/opt/gnurl
sudo make install
export PATH=/opt/gnurl/bin:$PATH


blueText "Install Gnunet..."
cd /opt/downl/gnunet
sudo mkdir /opt/gnunet
./bootstrap
./configure --prefix=/opt/gnunet
sudo make install
export PATH=/opt/gnunet/bin:$PATH


blueText "Add current user to gnunet group and modify permissions..."
sudo adduser $SUDO_USER gnunet
sudo chown -R gnunet:gnunet /opt/gnurl
sudo chown -R gnunet:gnunet /opt/gnunet


blueText "Install MHD..."
cd /opt/downl/libmicrohttpd-0.9.62/
sudo mkdir /opt/mhd
./configure --prefix=/opt/mhd
make install
sudo chown -R $SUDO_USER:$SUDO_USER /opt/mhd
export LD_LIBRARY_PATH=/opt/mhd/lib


blueText "Add env scripts to profile.d"
echo 'export PATH=/opt/gnurl/bin:$PATH' > /etc/profile.d/gnurl_env.sh
echo 'export PATH=/opt/gnunet/bin:$PATH' > /etc/profile.d/gnunet_env.sh
echo 'export LD_LIBRARY_PATH=/opt/mhd/lib:$LD_LIBRARY_PATH' > /etc/profile.d/mhd_env.sh
echo 'export LD_LIBRARY_PATH=/opt/gnunet/lib:$LD_LIBRARY_PATH' > /etc/profile.d/gnunet_lib.sh


blueText "Install Postgressql with Adminstudio"
sudo apt install curl ca-certificates
curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
sudo apt install postgresql-11 pgadmin4
echo 'Run'
echo 'sudo passwd postgres'
echo '1234qwer'
echo 'sudo -u postgres psql'
echo  '\password postgres'